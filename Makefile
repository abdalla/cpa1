Benchmarkanalyse = ./
sardToBenchmarks:
	python3 sardToBenchmarks.py

checkTool:
	cd CPAchecker-1.9-unix && python3 scripts/benchmark.py ../Benchmarkanalyse/xmlDateien/C_Programme_XML.xml

analyzeBenchmarkResults:
	python3 analyzeBenchmarkResults.py
