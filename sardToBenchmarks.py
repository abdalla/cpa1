import os
import re
import yaml
from pathlib import Path
from xml.dom import minidom
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import xml.etree.cElementTree as ET

CWE= dict()
cweLog = dict()
cweNummer = list()
cweOrdner = list()
cweSort = []
#Manifest XML die Auserhalb CPAchecker liegen
manifest100 = Path('.'+os.sep+'manifest-100-OxcFXz.xml')
manifest101 = Path('.'+os.sep+'manifest-101-z8RLtQ.xml')
my_root = Element('benchmark', {'tool':"cpachecker", 'timelimit':"900 s", 'hardtimelimit':"960 s",'memlimit':"2 GB",'cpuCores':"2"})
ordnerName = ["excelDateien","Yml_Dateien","SetDateien","xmlDateien","LogDateien","ChangeProgram"]
hauptOrdner = "Benchmarkanalyse"
hauptOrdnerPfad = Path("."+os.sep+hauptOrdner)
sardConf = ["smg","svcomp20"]
sardXML = ["100","101","100_verifikation","101_verifikation"]
sardZeit = []
spezi = dict()
spezifikationen = ["unreach-call.prp","valid-memsafety.prp"]

def ManifestXmlAufruf(rootID):
	""" Die Funktion ManifestXmlAufruf ruft die XML Dateien auf, die mit dem jeweiligen Testsuit übergeben wurde. """
	for testcase in rootID.findall('testcase'):
		testid = testcase.get('testsuiteid')
		if testid not in CWE.keys():
			cweOrdner.append(testid)
			CWE[testid]= dict()
	for fileid in rootID.findall('testcase'+os.sep+'file'):
		file = fileid.get('path')
		for flawid in fileid:
			flaw = flawid.get('name')
			if flaw not in CWE[testid].keys():
				CWE[testid][flaw]=list()
			CWE[testid][flaw].append(file)

def exisitiertManifest():
	""" Die Funktion existiertManifest überprüft, ob ein Manifest bzw. ein XML vorhanden ist, denn wir dann der Funktion ManifestXmlAufruf übergeben."""
	if manifest100.exists():
		tree = ET.ElementTree(file='manifest-100-OxcFXz.xml')
		root = tree.getroot()
		ManifestXmlAufruf(root)
	if manifest101.exists():
		tree2 = ET.ElementTree(file='manifest-101-z8RLtQ.xml')
		root2 = tree2.getroot()
		ManifestXmlAufruf(root2)

def SpezifikationZuweisung():
	"""Die Funktion SpezifikationZuweisung, übergibt zur jeder CWE den jeweiligen passende Spezifikation."""
	for cwe in CWE['100'].keys():
		cweNum = cwe.split(":")
		spezi[cweNum[0]] = list()
		cweNummer.append(cweNum[0])
		if re.match("CWE-121.*",cwe) or re.match("CWE-122.*",cwe) or re.match("CWE-170.*",cwe) or re.match("CWE-244.*",cwe) or re.match("CWE-251.*",cwe) or re.match("CWE-259.*",cwe) or re.match("CWE-367.*",cwe) or re.match("CWE-401.*",cwe) or re.match("CWE-412.*",cwe) or re.match("CWE-415.*",cwe) or re.match("CWE-416.*",cwe) or re.match("CWE-457.*",cwe) or re.match("CWE-468.*",cwe) or re.match("CWE-476.*",cwe):
				spezi[cweNum[0]].append('valid-memsafety.prp')
		elif re.match("CWE-489.*",cwe):
				spezi[cweNum[0]].append('unreach-call.prp')
		elif re.match("CWE-078.*",cwe) or re.match("CWE-080.*",cwe) or re.match("CWE-089.*",cwe) or re.match("CWE-099.*",cwe) or re.match("CWE-134.*",cwe):
				spezi[cweNum[0]].append("Taint-Analyse fehlt")
		elif re.match("CWE-391.*",cwe):
				spezi[cweNum[0]].append("Falsche CWE zugewiesen. Richtige : CWE-369 Divide by Zero")


def OrdnerErstellen():
	"""Die Funktion OrdnerErstellen, erstellt verschiedene Ordern. """
	if not(hauptOrdnerPfad.exists()):
		os.mkdir(hauptOrdner)
	for name in ordnerName:
		ordner = Path(hauptOrdner+os.sep+name)
		if not(ordner.exists()):
			os.mkdir(os.path.join(hauptOrdner,name))
		if name == "Yml_Dateien":
			for unterOrdnerName in sardXML:
				for cwe in spezi.keys():
					unterOrdner = Path(hauptOrdner+os.sep+name+os.sep+unterOrdnerName)
					if not(unterOrdner.exists()):
						os.mkdir(os.path.join(hauptOrdner+os.sep+name,unterOrdnerName))
					elif unterOrdner.exists():
						for cwe in cweNummer:
							cweNumPfad = Path(hauptOrdner+os.sep+name+os.sep+unterOrdnerName+os.sep+cwe)
							if unterOrdnerName == "100_verifikation" or unterOrdnerName == "101_verifikation":
								if cwe == "CWE-489":
									if not(cweNumPfad.exists()):
										os.mkdir(os.path.join(unterOrdner,re.sub(" ","",cwe)))
							elif not(cweNumPfad.exists()):
									os.mkdir(os.path.join(unterOrdner,re.sub(" ","",cwe)))
def changeCprogram():
	""" Die Funktion changeCprogram verändert bestimmte C-Programme, in dem es eine Verifikation im Code hinzufügt. Diese Veränderäte Dateien werden dann in Ordner 000/149/ChangeProgram gespeichert.
		cpro = eine Liste von C-Programme die sich in Manifest 100 und 101 befinden
		cCode = sind die einzelenen C-Programme mit ihrem Verzeichnis aus der Liste cpro
		name = ist der Name vom C-programm. Beispiel cCode = 000/149/fmt-bad.c dann ist name = fmt-bad.c
	 """
	for ordner in CWE.keys():
		for cwe, cpro in CWE[ordner].items():
			if cwe == "CWE-489: Leftover Debug Code":
				for cCode in cpro:
					name = cCode.split(os.sep)
					with open(cCode, encoding = "ISO-8859-1") as inputfile:
						with open (hauptOrdner+os.sep+"ChangeProgram"+os.sep+name[3],"w") as outputfile:
							for line in inputfile:
								outputfile.write(re.sub("promote_root()","__VERIFIER_error",line))
	return name[3]

def CreatSetSpeziFile():
	""" Die Funktion CreatSetSpeziFile, erstellt ein unterOrdner für den Ordner SetDateien. """
	for ordner in sardXML:
		for cwe in CWE['100'].keys():
			cweName = cwe.split(":")
			for spezifikation in spezi[cweName[0]]:
				spezifikationName = spezifikation.split(".")
				if spezifikation in spezifikationen:
					if ordner == "100" or ordner == "101":
						creatfile = open(hauptOrdner+os.sep+"SetDateien"+os.sep+ordner+"_"+spezifikationName[0]+".set","w")
						creatfile.close()
					if spezifikation == 'unreach-call.prp':
						if ordner == "100_verifikation" or ordner == "101_verifikation":
							creatfile = open(hauptOrdner+os.sep+"SetDateien"+os.sep+ordner+"_"+spezifikationName[0]+".set","w")
							creatfile.close()

def yml_dump(filepath,data,cweDatei,ordner):
	""" Die Funktion yml_dump, befüllt die YmlDateien"""
	ordnerPfad = hauptOrdner+os.sep+"Yml_Dateien"+os.sep+ordner+os.sep+re.sub(" ","",cweDatei)+os.sep+filepath
	with open(ordnerPfad, "w",encoding = 'utf8') as file_descriptor:
		yaml.dump(data,file_descriptor,default_flow_style=False)


def allgemeineAbfrage():
	""" Die Funktion allgemeieneAbfrage, erstellt eine allgmeine Abfrage für zwei unterschiedliche Funktionen, die jedoch die gleiche Abfrage haben."""
	for ordner in sardXML:
		for cwe in spezi.keys():
			for spezifikation in spezi[cwe]:
				if ordner == "100" or ordner == "101":
					setDateinBefuellung(ordner,spezifikation,cwe)
					yml_items(ordner,cwe,spezifikation,".."+os.sep+".."+os.sep+".."+os.sep+".."+os.sep)
				if spezifikation == 'unreach-call.prp':
					if ordner == "100_verifikation" or ordner == "101_verifikation":
						if cwe == "CWE-489":
							setDateinBefuellung(ordner,spezifikation,cwe)
							yml_items(ordner,cwe,spezifikation,".."+os.sep+".."+os.sep+".."+os.sep+".."+os.sep+hauptOrdner+os.sep+"ChangeProgram"+os.sep)

def ymlData(pfad,spezifikation,bool):
	""" Die Funktion ymlData, erstellt ein Data dem man dann dem yml übergen kann, genauer gesagt, beschreibt es wie die yml befüllt werden soll.""" 
	data = {'format_version':'1.0',
		'input_files':pfad,
		'properties': [{'property_file':"../../../../sv-benchmarks/"+ spezifikation,'expected_verdict': bool}]
			}
	return data

def yml_items(ordner,cwe,spezifikation,pfadteil):
	"""Die Funktion yml_items, kategoresiert  yml und cwe zu den zugehörigen Ordner. """
	for ordnerNummer in CWE.keys():
		for cweDict, cProgramme in CWE[ordnerNummer].items():
			cweNummer = cweDict.split(":")
			if cwe == cweNummer[0]:
				for cDateien in cProgramme:
					cDatei = cDateien.split(os.sep)
					datei = cDatei[3].split(".")
					if ordnerNummer == "100":
						bool = False
						filepath = datei[0] +".yml"

						if ordner == "100_verifikation":
							pfad = pfadteil+cDatei[3]
							data = ymlData(pfad,spezifikation,bool)
							yml_dump(filepath,data,cwe,ordner)
						elif ordner == '100':
							pfad = pfadteil+cDateien
							data = ymlData(pfad,spezifikation,bool)
							yml_dump(filepath,data,cwe,ordner)
					elif ordnerNummer == "101":
						bool = True
						filepath = datei[0] +".yml"
						if ordner == "101_verifikation":
							pfad = pfadteil+cDatei[3]
							data = ymlData(pfad,spezifikation,bool)
							yml_dump(filepath,data,cwe,ordner)
						elif ordner == '101':
							pfad = pfadteil+cDateien
							data = ymlData(pfad,spezifikation,bool)
							yml_dump(filepath,data,cwe,ordner)

def setDateinBefuellung(ordner,spezifikation,cwe):
	""" Die Funktion setDateienBefuellung, befuellt Dateien mit der Endung .set. In diesen Dateien, steht drin wo sich die YML Dateien befinden.""" 
	speziName = spezifikation.split(".")
	if spezifikation in spezifikationen:
		with open(hauptOrdner+os.sep+"SetDateien"+os.sep+ordner+"_"+speziName[0]+".set","r") as inputfile:
			if ".."+os.sep+hauptOrdner+os.sep+"Yml_Dateien"+os.sep+ordner+os.sep+re.sub(" ","",cwe)+os.sep+"*.yml"+"\n" not in inputfile.read():
				with open(hauptOrdner+os.sep+"SetDateien"+os.sep+ordner+"_"+speziName[0]+".set","a") as file:
					file.write(".."+os.sep+"Yml_Dateien"+os.sep+ordner+os.sep+re.sub(" ","",cwe)+os.sep+"*.yml"+"\n")


def prettify(elem):
	""" Die Funktion prettify, sorgt für überblickbare XML Datei."""
	rough_string = ElementTree.tostring(elem, 'utf-8')
	reparsed = minidom.parseString(rough_string)
	return reparsed.toprettyxml(indent="  ")


def xmlBody(myroot,ord,spez,path,run):
	"""Die Funktion xmlBody beschreibt was genau in den jeweiligen XML Dateien stehen soll."""
	child = SubElement(myroot,'rundefinition',{'name':run+"."+ord})
	SubElement(child,'option',{'name':"-"+run})

	child1 = SubElement(child,'option',{'name':"-setprop"})
	child1.text = 'solver.solver=SMTInterpol'

	child2 = SubElement(child,'option',{'name':"-setprop"})
	child2.text = 'cpa.predicate.encodeBitvectorAs=INTEGER'

	child3 = SubElement(child,'option',{'name':"-setprop"})
	child3.text = 'cpa.predicate.encodeFloatAs=RATIONAL'

	SubElement(child,'option',{'name':"-preprocess"})

	child4 = SubElement(child,'option',{'name':"-setprop"})
	child4.text = 'limits.time.cpu=900s'

	child5 = SubElement(child,'option',{'name':"-heap"})
	child5.text='13000M'

	child6 = SubElement(child,'option',{'name':"-setprop"})
	child6.text='cpa.predicate.memoryAllocationsAlwaysSucceed=true'

	child7 = SubElement(child,'tasks',{'name':ord})
	subChild1 = SubElement(child7,'includesfile')
	subChild2 = SubElement(child7,'propertyfile')

	subChild1.text = ".."+os.sep+"SetDateien"+os.sep+path
	subChild2.text = ".."+os.sep+".."+os.sep+"sv-benchmarks"+os.sep+spez

	tree = prettify(myroot)
	with open (hauptOrdner+os.sep+"xmlDateien"+os.sep+"C_Programme_XML.xml","w") as files:
		files.write(tree)

def GenerateXML():
	"""Die Funktion GenerateXML, ersellt die XML Datei."""
	for spez in spezifikationen:
		spezi = spez.split(".")
		for eigenschaftsOrdner in sardXML:
			if spez == 'unreach-call.prp':
				xmlBody(my_root,eigenschaftsOrdner,spez,eigenschaftsOrdner+"_"+spezi[0]+".set","svcomp20")
			elif spez == 'valid-memsafety.prp':
				if eigenschaftsOrdner == "100" or eigenschaftsOrdner == "101":
					xmlBody(my_root,eigenschaftsOrdner,spez,eigenschaftsOrdner+"_"+spezi[0]+".set","smg")


exisitiertManifest()
SpezifikationZuweisung()
OrdnerErstellen()
changeCprogram()
CreatSetSpeziFile()
allgemeineAbfrage()
GenerateXML()

