#!/usr/bin/python
#-*- coding:utf-8 -*-

import bz2
import collections
import datetime
import glob
import re
import xlsxwriter
import subprocess
import os
import xlrd
import zipfile
import zlib
from datetime import date
from itertools import product
from pathlib import Path
from xlutils.copy import copy
from xml.dom import minidom
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import xml.etree.cElementTree as ET

CWE= dict()
cweLog = dict()
cweNummer = list()
cweOrdner = list()
cweSort = []
exelDatei = Path("."+os.sep+"exelDateien")
exelSpalte = ["CWE","Spezifikation","Anzahl C-Programme","Wahr","Falsch","Unbekannt","Unbekannt Ausgabe"]
hauptOrdner = "Benchmarkanalyse"
#Manifest XML die Auserhalb CPAchecker liegen
manifest100 = Path('.'+os.sep+'manifest-100-OxcFXz.xml')
manifest101 = Path('.'+os.sep+'manifest-101-z8RLtQ.xml')
sardConf = ["smg","svcomp20"]
sardXML = ["100","101","100_verifikation","101_verifikation"]
sardZeit = []
spezi = dict()
spezifikationen = ["unreach-call.prp","valid-memsafety.prp"]

def ManifestXmlAufruf(rootID):
	""" Die Funktion ManifestXmlAufruf ruft die XML Dateien auf, die mit dem jeweiligen Testsuit übergeben wurde. """
	for testcase in rootID.findall('testcase'):
		testid = testcase.get('testsuiteid')
		if testid not in CWE.keys():
			cweOrdner.append(testid)
			CWE[testid]= dict()
	for fileid in rootID.findall('testcase'+os.sep+'file'):
		file = fileid.get('path')
		for flawid in fileid:
			flaw = flawid.get('name')
			if flaw not in CWE[testid].keys():
				CWE[testid][flaw]=list()
			CWE[testid][flaw].append(file)

def exisitiertManifest():
	""" Die Funktion existiertManifest überprüft, ob ein Manifest bzw. ein XML vorhanden ist, denn wir dann der Funktion ManifestXmlAufruf übergeben."""
	if manifest100.exists():
		tree = ET.ElementTree(file='manifest-100-OxcFXz.xml')
		root = tree.getroot()
		ManifestXmlAufruf(root)
	if manifest101.exists():
		tree2 = ET.ElementTree(file='manifest-101-z8RLtQ.xml')
		root2 = tree2.getroot()
		ManifestXmlAufruf(root2)

def SpezifikationZuweisung():
	"""Die Funktion SpezifikationZuweisung, übergibt zur jeder CWE den jeweiligen passende Spezifikation."""
	for cwe in CWE['100'].keys():
		cweNum = cwe.split(":")
		spezi[cweNum[0]] = list()
		cweNummer.append(cweNum[0])
		if re.match("CWE-121.*",cwe) or re.match("CWE-122.*",cwe) or re.match("CWE-170.*",cwe) or re.match("CWE-244.*",cwe) or re.match("CWE-251.*",cwe) or re.match("CWE-259.*",cwe) or re.match("CWE-367.*",cwe) or re.match("CWE-401.*",cwe) or re.match("CWE-412.*",cwe) or re.match("CWE-415.*",cwe) or re.match("CWE-416.*",cwe) or re.match("CWE-457.*",cwe) or re.match("CWE-468.*",cwe) or re.match("CWE-476.*",cwe):
				spezi[cweNum[0]].append('valid-memsafety.prp')
		elif re.match("CWE-489.*",cwe):
				spezi[cweNum[0]].append('unreach-call.prp')
		elif re.match("CWE-078.*",cwe) or re.match("CWE-080.*",cwe) or re.match("CWE-089.*",cwe) or re.match("CWE-099.*",cwe) or re.match("CWE-134.*",cwe):
				spezi[cweNum[0]].append("Taint-Analyse fehlt")
		elif re.match("CWE-391.*",cwe):
				spezi[cweNum[0]].append("Falsche CWE zugewiesen. Richtige : CWE-369 Divide by Zero")

def checkTool():
	"""Die Funktion checkTool, überpfüft die einzelnen C-Programme in dem TOOL CPAchecker."""
	# Um das Richtige Datei zufinden, wird die Zeit berechenet
	os.chdir(os.getcwd()+os.sep+"CPAchecker-1.9-unix")
	now = datetime.datetime.today()

	max = now.strftime('%H%M')
	rechenzeit = 6000
	while rechenzeit != 0 :
		rechenzeit = rechenzeit - 60
		zeit = now - datetime.timedelta(0,rechenzeit)
		sardZeit.append(zeit.strftime('%H%M'))

def bz2entpacken():
	""" Die Funktion bz2entpacken, enpackt die erstellten bz2 Dateien."""
	for ordnerBZ2 in sardXML:
		for zeit in sardZeit:
			for conf in sardConf:
				bz2Xml = "./test/results/C_Programme_XML."+str(date.today())+"_"+zeit+".results."+conf+"."+ordnerBZ2+"."+ordnerBZ2+".xml.bz2"
				xml = "./test/results/C_Programme_XML."+str(date.today())+"_"+zeit+".results."+conf+"."+ordnerBZ2+"."+ordnerBZ2+".xml"
				xmlPfad = Path(xml)
				bz2Pfad = Path(bz2Xml)
				if bz2Pfad.exists():
					with bz2.BZ2File(bz2Xml, "rb") as bz2Datei:
						zeilen = bz2Datei.readlines()
						with open(xml,"w") as new_xml:
							for zeile in zeilen:
								new_xml.write(zeile.decode("utf-8")+"\n")

def logEnpacken():
	""" Die Funktion logEnpacken entpackt die Log Datien und speichert sie in Ordner LogDateien. """
	for logOrdner in sardXML:
		for logZeit in sardZeit:
			log =  "."+os.sep+"test"+os.sep+"results"+os.sep+"C_Programme_XML."+str(date.today())+"_"+logZeit+".logfiles.zip"
			logPfad = Path(log)
			if logPfad.exists():
				zip_ref = zipfile.ZipFile(log)
				zip_ref.extractall(".."+os.sep+hauptOrdner+os.sep+"LogDateien")

def exel_loader():
	""" Die Funtkion  exel_loader erstellt eine exel Datei mit Überschriftungen. Die einzelenen werte werden später hinzugefügt."""
	for ordner in sardXML:
		with xlsxwriter.Workbook(".."+os.sep+hauptOrdner+os.sep+"excelDateien"+os.sep+"CWE-Ausgaben_"+ordner+".xlsx") as outWorkbook:
			outSheet = outWorkbook.add_worksheet()
			for pos in range(len(exelSpalte)):
				outSheet.write(0,pos,exelSpalte[pos])

def exelWerte():
	""" Die Funktion exelWerte befüllt die Exel Tabelle mit den enstandenen ergebnisse."""
	for cwe in spezi.keys():
		cweSort.append(cwe)
		cweSort.sort()
	for ordName, zeit, conf in product(sardXML, sardZeit, sardConf):
		cweExcel = dict()
		cproExcel = dict()
		entpackteXml = "test"+os.sep+"results"+os.sep+"C_Programme_XML."+str(date.today())+"_"+str(zeit)+".results."+conf+"."+ordName+"."+ordName+".xml"
		entpackteXmlPfad = Path(entpackteXml)
		if entpackteXmlPfad.exists():
			tree = ET.ElementTree(file=entpackteXml)
			root = tree.getroot()
			for run in root.findall('run'):
				ordner = run.get('name')
				ordSplit =ordner.split(os.sep)
				cweExcel[ordSplit[6]] = list()
			for runClone in root.findall('run'):
				ordnerClone = runClone.get('name')
				cproClone = runClone.get('files')
				cpClone = cproClone.split(os.sep)
				if "ChangeProgram" in cproClone:
					cClone = cpClone[5].split("]")
				else:
					cClone = cpClone[6].split("]")

				ordClone =ordnerClone.split(os.sep)
				cweExcel[ordClone[6]].append(cClone[0])
				cproExcel[ordClone[6]+os.sep+cClone[0]]=list()
				for column in runClone:
					title = column.get('title')
					if title == "status":
						ergebniss = column.get('value')
						cproExcel[ordClone[6]+os.sep+cClone[0]].append(ergebniss)

			rb = xlrd.open_workbook(".."+os.sep+hauptOrdner+os.sep+"excelDateien"+os.sep+"CWE-Ausgaben_"+ordName+".xlsx")
			path=".."+os.sep+hauptOrdner+os.sep+"excelDateien"+os.sep+"CWE-Ausgaben_"+ordName+".xlsx"
			wb = copy(rb)
			w_sheet = wb.get_sheet(0)

			for pos in range(len(cweSort)):
				w_sheet.write(pos+1,0,cweSort[pos])
				logPfad = ".."+os.sep+hauptOrdner+os.sep+"LogDateien"+os.sep+"C_Programme_XML."+str(date.today())+"_"+str(zeit)+".logfiles"
				if ordName == '100' or ordName =='101':
					for cwe,cpro in CWE[ordName].items() :
						cweSplit = cwe.split(":")
						if cweSplit[0] == cweSort[pos]:
							cweLog[cwe] =  list()
							for c in cpro:
								cp = c.split(os.sep)
								cd = cp[3].split(".")
								logAusgabePfad = logPfad+os.sep+conf+"."+ordName+"."+cd[0]+".yml.log"
								logPath = Path(logAusgabePfad)
								if logPath.exists():
									with open(logAusgabePfad,"r") as inputfile:
										file = inputfile.readlines()
										for line in file:
											if "Error: Unknown function" in line:
												issue = line.split()[3]
												cweLog[cwe].append(issue)
							fehler = " "
							for iss in cweLog[cwe]:
								fehler = fehler+iss+", "
								w_sheet.write(pos+1,6,fehler)
					for spezCwe in spezi.keys():
						if spezCwe == cweSort[pos]:
							for spez in spezi[spezCwe]:
								w_sheet.write(pos+1,1,spez)
					for cwe, cpro in cweExcel.items():
						wertCol = collections.Counter()
						if cwe == cweSort[pos]:
							w_sheet.write(pos+1,2,len(cweExcel[cwe]))
							for cp,value in cproExcel.items():
								for cliste in cpro:
									cSplit = cp.split(os.sep)
									if cliste == cSplit[1]:
										for v in value:
											if v == 'ERROR':
												wertCol['ERROR']+=1
												w_sheet.write(pos+1,5,str(wertCol['ERROR']))
											elif v == 'true':
												wertCol['true']+=1
												w_sheet.write(pos+1,3,str( wertCol['true']))
											else:
												wertCol['false']+=1
												w_sheet.write(pos+1,4,str(wertCol['false']))
			wb.save(path)


exisitiertManifest()
SpezifikationZuweisung()
checkTool()
bz2entpacken()
logEnpacken()
exel_loader()
exelWerte()

