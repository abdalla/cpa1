# Real-World Requirements for Software Analysis
Die Programmiersprachen, beinhalten sicherheitslücken. Um diese Sicherheitslücken zu finden und zu korrigieren, wurde ein Tool namens CPAchecker entwickelt.
Um den CPAchecker auf seiner Richtigkeit zu überprüfen, wurden verschiedene C-Programme entwickelt, die jeweils verschiedene Sicherheitslücken beinhalten. 
Diese C-Prgramme, werden auf dem CPAchecker ausgeführt, um zu erkenne, ob das Tool in der Lage ist, diese Fehler zu erkennen. 
Da es mehrere C-Programme existieren, benötigen wir ein Script, das die C-Programme mit den nötigen Einstellungen an dem CPAchecker ausführt. 
Hierfür wurden zwei Python Skripte erstellt, namens sardToBenchmarks.py, analyzeBenchmarkResults.py.


## CPAchecker
Das CPAchecker, ist ein Tool, der die einzelnen C-Programme analysiert und bewertet. Mit den erhaltenen Ergebnisse, könnte man den CPAchekcker korrigieren
als auch die Sicherheitslücken minimieren. Um das Programm auszuführen, müssen sie das CPAchecker installieren. Bitte suchen Sie sich einen geeigneten Ort,
wo sie den CPAchecker installieren wollen, denn die Skripte werden später auf der gleichen Ebene installiert. Wie Sie das CPAchecker einrichten, finden Sie auf 
dieser Git Seite ( https://github.com/sosy-lab/cpachecker/blob/trunk/doc/Developing.md ).

* [CPAchecker](https://cpachecker.sosy-lab.org/download.php)

## C-Programme
Jedes einzelne C-Programm enthält Sicherheitslücken, die jeweils kategorisiert werden in CWE. Das CWE kategorisiert die Sicherheitslücken, z. B. enthält das 
CWE-244 Sicherheitslücken, bei dem das Heap-Speicher unsachgemäß gelöscht werden, bevor es freigegeben wird. C-Programme, die solchen Sicherheitslücken 
beinhalten, werden unter CWE-244 kategorisiert. Um zu überprüfen, ob das Tool die C-Programme tatsächlich richtig bewertetet, wurden zwei arten von C-Programme 
entwickelt, einmal C-Programme mit Sicherheitslücken und einmal ohne Sicherheitslücken. C-Programme mit Sicherheitslücken, befinden sich in Ordner 100
und C-Programme ohne Sicherheitslücken befinden sich in Ordner 101. Um das Script später ausführen zu können, installieren sie bitte die C-Programme, 
die jeweils unter  "Test Suite ID", Ordner 100 und 101 kategorisiert sind. Sobald Sie Ordner 100 und 101 installiert haben, entpacken Sie bitte, die Dateien. 
Bitte beachten, dass sich Ordner 100 und 101 sich auf der gleichen Ebene befinden müssen, wie in CPAchecker. Sobald Sie dies entpackt haben, müssten Sie auf der 
gleichen eben wie CPAchecker, jeweils zwei neu Dateien finden, bzw. ein Ordner und ein XML. Bitte beachten Sie, dass das neue Ordner "000" und das Manifest 
XML auf der gleichen ebene liegen müssen wie das CPAchecker. 

* [C-Programme](https://samate.nist.gov/SRD/testsuite.php)

## Python Script
Sobald Sie Ordner 100 und 101, als auch das CPAcheker erfolgreich installiert haben, können Sie nun die zwei Python Skripte und das Makefile installieren. 
Das Python Skript sardToBenchmarks.py, erstellt die notwendigen Dateien. 
Hier werden Benchmarkanalyse, SetDateien, Yml_Datien, xmlDateien, LogDateien und ChangeProgram erstellt. Das Python Skript analyzeBenchmarkResults.py, 
analysiert die entstandenen Ergebnisse, in dem es die entstandenen bz2 und Log Dateien entpackt und die jeweiligen Ergebnisse in einer Excel Tabelle befüllt.
Das Makefile ruft die Python Scripts unabhängig voneinander auf. Bitte beachten Sie, dass auch hier die Python Skripte als auch das Makefile auf die gleiche 
Ebene liegen sollte, wie das CPAchecker. Sobald Sie auch das erledigt haben, müssen Sie noch folgende Pakete fürs Python Skript Instalieren: 

* xlrd
* date
* minidom
* yaml
* xlutils
* xlsxwriter 
* pyaml




### Benchmarkanalyse
In dem Ordner Benchmarkanalyse befinden sich die folgenden Dateien:

* xmlDateien
* SetDateien
* Yml_Dateien
* excelDateien

### xmlDateien
In dem Ordner xmlDateien, befindet sich ein XML Datei, die später ausgeführt wird. In diesem XML Datei wird beschrieben, wie die C-Programme ausgeführt werden sollten
und wo sie sich befinden. In dem Fall stehen die SetDateien in dem XML Datei drin, der beschreibt, wo sich die C-Programme befinden. 
Dies sorgt für eine bessere Übersicht. 

### SetDatein
In dem Ordner SetDateien, befinden sich Dateien mit der Endung ".set", in dem drin steht, wo sich die YmlDateien befinden.

### Yml_Datein
In dem Ordner Yml_Datien, befinden sich die Yml Dateien, die beschreiben, wo sich die C-Programme befinden, mit welcher wichtigen Einstellung die C-Programme
ausgeführt werden sollten und welche Werte wir erwarten. 

### ChangeProgram
Es gibt ein paar C-Programme, die Manuel verändert wurden, um festzustellen, ob der Fehler, tatsächlich am Code liegt oder nicht. Diese veränderten C-Programme,
befinden sich in ChangeProgram. 
### LogDateien
Der Ordner LogDateien, beinhaltet, die erstellten Log Dateien. Nach dem die C-Programme an das Tool ausgeführt wurde, erhalten wir Log Dateien, in dem die 
Ausgabe drin steht. Diese Log Dateien werden dann, in dem Ordner LogDateien gespeichert. 

### excelDateien
In dem Ordner excelDateien, befinden sich Excel Tabellen, die für die jeweiligen Ordner (100 und 101) erstellt wurden. 
In diesem Excel Tabelle, stehen die Ergebnisse übersichtlich drin.

# Ausführung
Sobald Sie alles richtig Installiert haben, führen sie bitte die Python Skripte durch, in dem Sie in ihrem Terminal folgendes schreiben :

`make sardToBenchmarks`

`make checkTool`

`make analyzeBenchmarkResults`

Bitte beachten Sie, das Sie die Folgenden Anweisung auch in der gleichen Reihenfolge durchführen. Make checkTool ünerprüft die einzelnen C-Programme mit hilfe des CPAchecker.

Nach dem dies getan haben, finden Sie das Ordner Benchmarkanalyse und die Folgenden unterdateien. 

